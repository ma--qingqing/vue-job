import VueScroller from 'vue-scroller'


const plugin1 = {
    install: function (Vue) {
        console.log('plugin1 start')
        console.log(Vue);
        //增强属性
        Vue.prototype.$class = 'class7';
        //全局的混入
        Vue.mixin({
            data: function () {
                return {
                    month: 15,
                    hope: '挣大钱',
                    pageSize: 20
                }
            }
        })
        //全局的指令.....

        Vue.use(VueScroller)
    }

}
export default plugin1